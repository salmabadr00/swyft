# swyft

Automated tests for swyft online ordering system

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Notes](#notes)

## General info
This project aims to execute automated tests on SWYFT online ordering system (http://35.233.12.50)
## Technologies
Project is created with:
* Cypress version: 3.4.1
* VSCode version: 1.38.1

## Setup
Installation:
```
Navigate to project folder
Install npm packages:
npm install

```
Running:
```
to run Cypress:
npm run cypress:open

A list of tests will be displayed,
then you can choose which tests you to run or choose run all specs at the top right

```
## Notes

* As per the shortage of time, Test execution report isnot generated
* All Docs related to the test activities can be found in the root directory of the project
* For the addService suite, "before" keyword wasnot behaving as expected so "beforeEach" is used instead. although it doesn't make sense to login the user before each test but due to lack of time, I couldn't make any further investegations about it